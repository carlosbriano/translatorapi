package com.softtek.translatorAPI;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.softtek.translator.controller.TranslatorController;
import com.softtek.translator.model.Request.TranslationRequest;
import com.softtek.translator.model.response.TranslationResponse;
import com.softtek.translator.service.TranslatorService;
import com.softtek.translator.service.impl.TranslatorServiceImpl;

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;



@RunWith(SpringJUnit4ClassRunner.class)
public class TranslateControllerTest {	
	
	@InjectMocks
	public TranslatorController translatorController;
	@Mock
	private TranslatorService translatorService;
	
	private MockMvc mockMvc;
	
	public void TranslateControllerTest() {} 
	
	@Before
	public void intializeMocks() {
		mockMvc = MockMvcBuilders.standaloneSetup(translatorController).build();
	}
	
	@Test
	public void callGoogleApi() throws Exception {
		String json= "{\"sourceLanguage\":\"es\", \"targetLanguage\":\"en\", \"textToTranslate\":[\"Hola\",\"mundo\"]}";
		TranslationRequest tranRequest= new TranslationRequest();
		String[] text= {"hola", "mundo"};
		String[] text2= {"hello", "world"};
		tranRequest.setSourceLanguage("en");
		tranRequest.setTargetLanguage("es");
		tranRequest.setTextToTranslate(text);
		TranslationResponse tranResponse= new TranslationResponse();
		tranResponse.setTranslations(text2);
		Mockito.when(translatorService.call(tranRequest)).thenReturn(tranResponse);
		mockMvc.perform(MockMvcRequestBuilders.post("/translations")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.characterEncoding("utf-8")
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)) 
					.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	
}