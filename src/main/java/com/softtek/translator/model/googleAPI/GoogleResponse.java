package com.softtek.translator.model.googleAPI;

public class GoogleResponse {
	
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
	
}
