package com.softtek.translator.model.googleAPI;

public class Data {
	
	private Translation[] translations;

	public Translation[] getTranslations() {
		return translations;
	}

	public void setTranslations(Translation[] translations) {
		this.translations = translations;
	}
	
}
