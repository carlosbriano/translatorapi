package com.softtek.translator.model.googleAPI;

public class Translation {
	private String translatedText;

	public String getTranslatedText() {
		return translatedText;
	}

	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}
	
	
}
