package com.softtek.translator.model.response;

import org.springframework.stereotype.Component;

@Component
public class TranslationResponse {
	
	private String[] translations;

	public String[] getTranslations() {
		return translations;
	}

	public void setTranslations(String[] translations) {
		this.translations = translations;
	}

}
