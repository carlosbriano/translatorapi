package com.softtek.translator.model.Request;

public class TranslationRequest {
	
	private String sourceLanguage;
	private String targetLanguage;
	private String[] textToTranslate;
	
	public String getSourceLanguage() {
		return sourceLanguage;
	}
	
	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}
	
	public String getTargetLanguage() {
		return targetLanguage;
	}
	
	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}

	public String[] getTextToTranslate() {
		return textToTranslate;
	}

	public void setTextToTranslate(String[] textToTranslate) {
		this.textToTranslate = textToTranslate;
	}
	
}
