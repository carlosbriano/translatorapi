package com.softtek.translator.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.translator.model.Request.TranslationRequest;
import com.softtek.translator.model.response.TranslationResponse;
import com.softtek.translator.service.TranslatorService;
@CrossOrigin(origins = "http://localhost:8072", maxAge = 3600)
@RestController
public class TranslatorController {
	
	@Autowired
	private TranslatorService translatorService;
	
    @PostMapping("/translations")
    public ResponseEntity getDiagnosis(@Valid @RequestBody TranslationRequest translationRequest) {
    	TranslationResponse translation = translatorService.call(translationRequest);
    		return ResponseEntity.ok().body(translation);
    }
	
}
