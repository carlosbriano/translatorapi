package com.softtek.translator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class TranslatorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TranslatorApiApplication.class);
	}

}
