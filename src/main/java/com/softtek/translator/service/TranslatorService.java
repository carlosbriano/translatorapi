package com.softtek.translator.service;

import com.softtek.translator.model.Request.TranslationRequest;
import com.softtek.translator.model.response.TranslationResponse;

public interface TranslatorService {
	
	public TranslationResponse call(TranslationRequest translationRequest);

}
