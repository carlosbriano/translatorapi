package com.softtek.translator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.softtek.translator.model.Request.TranslationRequest;
import com.softtek.translator.model.googleAPI.GoogleResponse;
import com.softtek.translator.model.response.TranslationResponse;
import com.softtek.translator.service.TranslatorService;
import com.softtek.translator.util.GoogleEndpoint;

@Service
public class TranslatorServiceImpl implements TranslatorService{
	
	@Autowired
	GoogleEndpoint googleEndpoint;
	@Autowired
	TranslationResponse translationResponse;
	
	public TranslationResponse call(TranslationRequest translationRequest) {
		String textToTranslate= joinText(translationRequest.getTextToTranslate());
		googleEndpoint.setText(textToTranslate);
		googleEndpoint.setSourceLanguage(translationRequest.getSourceLanguage());
		googleEndpoint.setTargetLanguage(translationRequest.getTargetLanguage());
		googleEndpoint.setFormat("text");
		RestTemplate restTemplate = new RestTemplate();
		GoogleResponse translatedResponse = restTemplate.postForObject(googleEndpoint.getUrl() , null, GoogleResponse.class );	
		String translatedText = translatedResponse.getData().getTranslations()[0].getTranslatedText();
		translationResponse.setTranslations(plainTextToVector(translatedText));
		return translationResponse;
	}
	
	
	private String joinText(String[] translations) {
		String joinedText="";
		for(String text:translations) {
			joinedText+=text + " // ";
		}
		return joinedText;		
	}
	
	private String[] plainTextToVector(String text) {
		return text.split("//");
	}
	
}

