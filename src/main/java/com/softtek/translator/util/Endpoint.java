package com.softtek.translator.util;

public class Endpoint {
	
	private Endpoint() {
		throw new IllegalStateException("Utility class");
	}
	
	public static final String GOOGLETRANSLATOR="https://translation.googleapis.com/language/translate/v2";
	
}
