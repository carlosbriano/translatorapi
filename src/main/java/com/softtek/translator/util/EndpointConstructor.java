package com.softtek.translator.util;

public interface EndpointConstructor {
	
	public String getUrl();
	
}
