package com.softtek.translator.util;
import org.springframework.stereotype.Component;

@Component
public class GoogleEndpoint implements EndpointConstructor {
	
	private static final String API_KEY= Enviroment.API_KEY;
	private String targetLanguage;
	private String sourceLanguage;
	private String text;
	private String format;
	private static final String BASEPATH=Endpoint.GOOGLETRANSLATOR;
	
	public String getUrl() {
		return BASEPATH.concat("?"+"q="+text+"&"+"target="+targetLanguage+"&"+"format="+format+"&"+"source="+sourceLanguage+"&"+
				"key="+API_KEY);
	}

	public String getTargetLanguage() {
		return targetLanguage;
	}

	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}

	public String getSourceLanguage() {
		return sourceLanguage;
	}

	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}


}
